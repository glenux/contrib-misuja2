(******************************************************************************)
(*      Copyright (c) 2007,2017 Sebastien MONDET                              *)
(*                                                                            *)
(*      Permission is hereby granted, free of charge, to any person           *)
(*      obtaining a copy of this software and associated documentation        *)
(*      files (the "Software"), to deal in the Software without               *)
(*      restriction, including without limitation the rights to use,          *)
(*      copy, modify, merge, publish, distribute, sublicense, and/or sell     *)
(*      copies of the Software, and to permit persons to whom the             *)
(*      Software is furnished to do so, subject to the following              *)
(*      conditions:                                                           *)
(*                                                                            *)
(*      The above copyright notice and this permission notice shall be        *)
(*      included in all copies or substantial portions of the Software.       *)
(*                                                                            *)
(*      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       *)
(*      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES       *)
(*      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND              *)
(*      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT           *)
(*      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,          *)
(*      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING          *)
(*      FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR         *)
(*      OTHER DEALINGS IN THE SOFTWARE.                                       *)
(******************************************************************************)

(** 
 
   OCaml types and functions providing high level access to a jack midi
   sequencer.

   @author S. Mondet

*)
module Sequencer = struct
  (** The sequencer object *)
  type t

  external make :
    name:string -> input_ports:string array -> output_ports:string array -> t
    = "ml_jackseq_make"
  (**
     The sequencer constructor
     should be called as  {[
       let my_seq =
         Sequencer.make "client_name"
           [| "input_port_A" ; "input_port_B" |]
           [| "out1" ; "out2" ; "outN" |]
       in
     ]}
  *)

  external close : t -> unit = "ml_jackseq_close"
  (** Close the sequencer ({i ie} jack client) *)

  external output_event :
    t -> port:int -> stat:int -> chan:int -> dat1:int -> dat2:int -> unit
    = "ml_jackseq_output_event_bytecode" "ml_jackseq_output_event"
  (** Put an event in the output buffer, it will be really output at next 
      jack frame (which means quasi immediately) *)

  external get_input :
    t -> (int * int * int * int * int) array
    = "ml_jackseq_get_input"
  (** Get all events in input buffer and clear it *)
end
