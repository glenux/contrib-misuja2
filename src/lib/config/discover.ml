module C = Configurator.V1

let () =
  C.main ~name:"jack" (fun c ->
      let default : C.Pkg_config.package_conf =
        {libs= ["-ljack"]; cflags= ["-I/usr/include/jack"]}
      in
      let conf =
        match C.Pkg_config.get c with
        | None ->
            default
        | Some pc -> (
          match C.Pkg_config.query pc ~package:"jack" with
          | None ->
              default
          | Some a ->
              a )
      in
      C.Flags.write_sexp "c_flags.sexp" conf.cflags ;
      C.Flags.write_sexp "c_library_flags.sexp" conf.libs )
